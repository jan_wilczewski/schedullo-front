import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-drag-drop-example',
  templateUrl: './drag-drop-example.component.html',
  styleUrls: ['./drag-drop-example.component.css']
})
export class DragDropExampleComponent implements OnInit {

  items = [
    {name: "Apple", type: "fruit"},
    {name: "Carrot", type: "vegetable"},
    {name: "Orange", type: "fruit"}];


  public droppedItems = [
    {name: "", type: ""}
  ];

  onItemDrop(e: any) {
    // Get the dropped data here
    this.droppedItems.push(e.dragData);
  }

  constructor() { }

  ngOnInit() {
  }

}
