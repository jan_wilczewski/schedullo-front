import {Component, OnInit} from '@angular/core';
import {DragulaService} from 'ng2-dragula';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})

export class TaskComponent implements OnInit {

  tasks = [
    {header: "TODO", title: "Add button", text: "Need to do this "},
    {header: "TODO", title: "Category list", text: "Need to do that"},
    {header: "TODO", title: "Backend connection", text: "And need to do that"}];

  public droppedTasks = [];


  // constructor(private dragulaService: DragulaService) {
  //   this.dragulaService.setOptions('bag-task1', {
  //     copy: true
  //   });
  // }

  constructor(private dragulaService: DragulaService) {
    dragulaService.dropModel.subscribe((value) => {
      this.onDropModel(value.slice(1));
    });
    dragulaService.removeModel.subscribe((value) => {
      this.onRemoveModel(value.slice(1));
    });
  }

  private onDropModel(args) {
    let [el, target, source] = args;
    // do something else
  }

  private onRemoveModel(args) {
    let [el, source] = args;
    // do something else
  }


  ngOnInit() {
  }

}
