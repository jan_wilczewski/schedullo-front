import { Component, OnInit } from '@angular/core';
import {TaskComponent} from '../task/task.component';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  columns: string [] = ['ToDo', 'In Progress', 'Approved'];


  constructor() { }

  ngOnInit() {
  }

}
