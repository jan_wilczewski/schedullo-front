import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { CategoriesComponent } from './categories/categories.component';
import { AddTaskButtonComponent } from './task/add-task-button/add-task-button.component';
import { DropDownMenuComponent } from './task/drop-down-menu/drop-down-menu.component';
import {FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {Ng2DragDropModule} from 'ng2-drag-drop';
import { DragDropExampleComponent } from './drag-drop-example/drag-drop-example.component';
import {TaskComponent} from './task/task.component';
import {DragulaModule, DragulaService} from 'ng2-dragula';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    CategoriesComponent,
    TaskComponent,
    AddTaskButtonComponent,
    DropDownMenuComponent,
    DragDropExampleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlexLayoutModule,
    Ng2DragDropModule.forRoot(),
    DragulaModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
